package appium.tutorial.android;


import appium.tutorial.android.util.ACCOUNT_TYPE;
import appium.tutorial.android.util.AppiumTest;
import appium.tutorial.android.util.Helpers;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidKeyCode;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.After;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.junit.Before;
import org.openqa.selenium.interactions.touch.LongPressAction;

import javax.lang.model.element.Element;
import javax.swing.text.View;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static appium.tutorial.android.util.Helpers.*;
//import static appium.tutorial.android.TestConfig;

/**
 * Created by asripath on 6/11/15.
 */
public class BVT extends AppiumTest {
    private String enter = "\n";
    private final String USER_AGENT = "Mozilla/5.0";
    private Integer httpErrorCode = 0;
    SmokeTest smokeTest = new SmokeTest();
    //ACCOUNT_TYPE account_type = new ACCOUNT_TYPE();

    private String username = ACCOUNT_TYPE.US1.getUsername();
    private String password = ACCOUNT_TYPE.US1.getPassword();


    public void signUp() throws Exception {
        button("GET STARTED").click();

        getViewById("com.starbucks.mobilecard:id/register_sign_in_register_button").click();
        Thread.sleep((500));
        String firstName = "American";
        String lastName = "Tester";

        driver.findElementById("com.starbucks.mobilecard:id/first_name").sendKeys(firstName + enter);
        driver.findElementById("com.starbucks.mobilecard:id/last_name").sendKeys(lastName + enter);

        Calendar timeSinceEpoch = Calendar.getInstance();
        String email = "a" + timeSinceEpoch.getTimeInMillis() + "@sbuxautomation.com";
        getViewById("com.starbucks.mobilecard:id/email").sendKeys(email + enter);


        takeScreenshot("accountInfo");
        String password = "Starbucks01*";
        driver.findElementById("com.starbucks.mobilecard:id/password").sendKeys(password + enter);
        getViewById("com.starbucks.mobilecard:id/confirm_password").sendKeys(password + enter);
        getViewById("com.starbucks.mobilecard:id/zip").sendKeys("98124");
        getViewById("com.starbucks.mobilecard:id/account_submit").click();
    }

    public void signUp(String firstName, String lastName, String email, String password, Integer zip) throws Exception {
        button("GET STARTED").click();

        getViewById("com.starbucks.mobilecard:id/register_sign_in_register_button").click();
        Thread.sleep((500));
        if (firstName.isEmpty()) {
            firstName = "Automation";
        }
        if (lastName.isEmpty()) {
            lastName = "Automation";
        }
        driver.findElementById("com.starbucks.mobilecard:id/first_name").sendKeys(firstName + enter);
        driver.findElementById("com.starbucks.mobilecard:id/last_name").sendKeys(lastName + enter);

        if (zip == null) {
            zip = 98134;
        }

        Calendar timeSinceEpoch = Calendar.getInstance();

        //In the event the developer doesn't want to or doesn't have a username and password
        if (email.isEmpty()) {
            email = "a" + timeSinceEpoch.getTimeInMillis() + "@sbuxautomation.com";
        }
        if (password.isEmpty()) {
            password = "Starbucks01*";
        }

        getViewById("com.starbucks.mobilecard:id/email").sendKeys(email + enter);
        takeScreenshot("accountInfo");
        driver.findElementById("com.starbucks.mobilecard:id/password").sendKeys(password + enter);
        getViewById("com.starbucks.mobilecard:id/confirm_password").sendKeys(password + enter);
        getViewById("com.starbucks.mobilecard:id/zip").sendKeys(zip.toString());
        getViewById("com.starbucks.mobilecard:id/account_submit").click();
    }

    @org.junit.Test
    public void signIn() throws Exception {
        button("GET STARTED").click();
        assert true : "SIGN UP";

        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();


        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/account_submit").click();
        assert true : "PAY";

    }


    //@org.junit.Test
    //TODO: Figure out text view
    public void forgotUsername() throws Exception {
        button("GET STARTED").click();
        assert true : "SIGN UP";

        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();
        //getViewById("com.starbucks.mobilecard:id/sign_in_forgot_username").click();
        //text("username").click();
        getViewById("com.starbucks.mobilecard:id/sign_in_forgot_un_pw_text").click();
        //getViewById("com.starbucks.mobilecard:id/forgot_username_email").click();
        //assert false :getViewById("com.starbucks.mobilecard:id/button_send").isEnabled();
        getViewById("com.starbucks.mobilecard:id/edit_text_combo_box_edit_box").sendKeys(username);
        getViewById("com.starbucks.mobilecard:id/button_send").click();
        assert true : "sent";
        assert true : "SHOW";
    }

    //@org.junit.Test
    //TODO: Figure out text view
    public void forgotPassword() throws Exception {
        button("GET STARTED").click();
        assert true : "SIGN UP";

        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();
        getViewById("com.starbucks.mobilecard:id/sign_in_forgot_username").click();
        assert true : "register";
        getViewById("com.starbucks.mobilecard:id/forgot_username_email").click();
        assert true : "account";
        //assert false :getViewById("com.starbucks.mobilecard:id/button_send").isEnabled();
        getViewById("com.starbucks.mobilecard:id/forgot_username_email").click();
        getViewById("com.starbucks.mobilecard:id/forgot_username_email").clear();
        getViewById("com.starbucks.mobilecard:id/forgot_username_email").sendKeys(username);
        getViewById("com.starbucks.mobilecard:id/button_send").click();


    }

    @org.junit.Test
    public void accessSignUpFromPayRewards() throws Exception {
        button("GET STARTED").click();
        assert true : "SIGN UP";
        getNavDrawer().click();
        Thread.sleep(500);
        text("REWARDS").click();
        assert true : "benefits";
        assert true : getViewById("com.starbucks.mobilecard:id/register_sign_in_register_button").isDisplayed();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_register_button").click();
        assert true : "email";
        back();
        back();
        getNavDrawer().click();
        Thread.sleep(500);
        text("GIFT").click();
        assert true : "brighten";
        assert true : getViewById("com.starbucks.mobilecard:id/register_sign_in_register_button").isDisplayed();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_register_button").click();
        assert true : "Name";
        back();
        back();
        getNavDrawer().click();
        Thread.sleep(500);
        text("SETTINGS");
        assert true : "Preferences";
        back();
        getNavDrawer().click();
        Thread.sleep(500);
        text("PAY").click();
        assert true : "drinks";
        assert true : getViewById("com.starbucks.mobilecard:id/register_sign_in_register_button").isDisplayed();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_register_button").click();
        assert true : "Name";
        back();
        back();
    }

    @org.junit.Test
    public void signOut() throws Exception {
        button("GET STARTED").click();
        assert true : "SIGN UP";

        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();

        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/account_submit").click();
        assert true : "PAY";
        getNavDrawer().click();
        Thread.sleep(500);
        text("SETTINGS").click();
        text("Security");
        scroll_to("Sign Out");
        getViewById("com.starbucks.mobilecard:id/settings_inner_fragment_signout_container").click();
        button("SIGN OUT").click();

    }

    @org.junit.Test
    public void addDigitalCard() throws Exception {

        signUp();
        getViewById("com.starbucks.mobilecard:id/register_card_get_started_button").click();
        getViewById("com.starbucks.mobilecard:id/register_digital_card").click();
        Thread.sleep(500);
        Assert.assertEquals("Reload Button should be seen", true, getViewById("com.starbucks.mobilecard:id/pay_fragment_reload_button").isDisplayed());
    }


    //@org.junit.Test
    //TODO: Figure out text view
    public void termsOfUseAndPrivacyPolicyInSignUp() throws Exception {
        button("GET STARTED").click();
        assert true : "SIGN UP";
        getViewById("com.starbucks.mobilecard:id/register_sign_in_register_button").click();
        Thread.sleep((500));
        scroll_to("Subscribe");

        text("Terms").click();
        Thread.sleep(500);
        assert true : getViewById("com.starbucks.mobilecard:id/termsSignUpFragment_webView");
        assert true : "APPLICATION";
        back();
        scroll_to("Subscribe");
        text("Privacy");

        Thread.sleep(500);
        driver.findElementByXPath("//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.TextView[3]\n").click();
        //text("Policy").click();
        assert true : "STATEMENT";

    }

    @org.junit.Test
    public void termsAndConditionsOnNoCardScreen() throws Exception {
        signUp();
        getViewById("com.starbucks.mobilecard:id/register_digital_card_terms_and_conditions").click();
        assert true : "USA";
    }

    //@org.junit.Test
    //TODO: Need to get Starbucks from any page
    public void sleepTesting() throws Exception {
        button("GET STARTED").click();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();

        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username + enter);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/account_submit").click();

        //Log.info("Sign up completed, on Pay screen");
        getViewById("com.starbucks.mobilecard:id/pay_fragment_reload_button");

        driver.sendKeyEvent(AndroidKeyCode.HOME);
        Thread.sleep((5000));
        driver.findElementById("com.sec.android.app.launcher:id/home_allAppsIcon").click();
        text("Starbucks").click();
        Thread.sleep((5000));
        getViewById("com.starbucks.mobilecard:id/pay_fragment_reload_button");
    }

    @org.junit.Test
    public void changePasscode() throws Exception {
        button("GET STARTED").click();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();

        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username + enter);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/account_submit").click();

        //Log.info("Sign up completed, on Pay screen");
        getViewById("com.starbucks.mobilecard:id/pay_fragment_reload_button");
        getNavDrawer().click();
        Thread.sleep(500);
        text("SETTINGS").click();
        scroll_to("Security");
        //scroll_to(getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock"));
        getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock_status").click();
        getViewById("com.starbucks.mobilecard:id/alert_dialog_positive_button").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        Thread.sleep(1000);
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();

        //getViewById("com.starbucks.mobilecard:id/settings_passcode_container");
        scroll_to("Security");
        getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock_status").click();
        getViewById("com.starbucks.mobilecard:id/passcode_dialog_change_passcode").click();

        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();

        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_2").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_3").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_4").click();

        Thread.sleep(1000);
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_2").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_3").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_4").click();

        //getViewById("com.starbucks.mobilecard:id/settings_passcode_container");
        scroll_to("Security");
        getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock_status").click();
        getViewById("com.starbucks.mobilecard:id/passcode_dialog_turn_passcode_off").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_2").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_3").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_4").click();

        //getViewById("com.starbucks.mobilecard:id/settings_passcode_container");


    }

    @org.junit.Test
    public void verifyAppVersionInSettings() throws Exception {
        button("GET STARTED").click();
        assert true : "SIGN UP";
        getNavDrawer().click();
        Thread.sleep(500);
        text("SETTINGS").click();
        assert true : "Version";
        assert true : getViewById("com.starbucks.mobilecard:id/settings_inner_fragment_version_text").isDisplayed();

        getNavDrawer().click();
        Thread.sleep(500);
        text("PAY").click();

        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username);
        getViewById("com.starbucks.mobilecard:id/account_submit").click();
        assert true : "VIEW";
        Thread.sleep(2000);
//        if(getViewById("com.starbucks.mobilecard:id/rewards_overlay_fragment_header_text").isDisplayed()){
//            getViewById("com.starbucks.mobilecard:id/rewards_overlay_close_button").click();
//        }

        String whatever = "//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.view.View[1]";
        if (!driver.findElements(By.xpath(whatever)).isEmpty()) {
            getViewById("com.starbucks.mobilecard:id/rewards_overlay_close_button").click();
        }


//        WebElement webElement =driver.findElementByXPath("//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.view.View[1]");
//            if(webElement.isDisplayed()){
//                getViewById("com.starbucks.mobilecard:id/rewards_overlay_close_button").click();
//            }
        getNavDrawer().click();
        Thread.sleep(500);
        text("SETTINGS").click();
        scroll_to("Version");
        //getViewById("com.starbucks.mobilecard:id/settings_passcode_container").click();
        assert true : getViewById("com.starbucks.mobilecard:id/settings_inner_fragment_version_text").isDisplayed();

    }

    @org.junit.Test
    public void barcode() throws Exception {
        button("GET STARTED").click();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();

        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username + enter);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/account_submit").click();

        //Log.info("Sign up completed, on Pay screen");
        getViewById("com.starbucks.mobilecard:id/pay_fragment_reload_button");
        getViewById("com.starbucks.mobilecard:id/pay_fragment_gift_card_image").click();
        driver.findElementById("com.starbucks.mobilecard:id/barcode_done_button").click();
        getViewById("com.starbucks.mobilecard:id/pay_fragment_reload_button");
    }

    @org.junit.Test
    public void rewardsBenefits() throws Exception {

        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//        if (checkConnection() == false) {
//            button("*****No Internet******" + httpErrorCode).click();
//        }
//        takeScreenshot("first");
//        signUp();
//
//        takeScreenshot("fourth");
//        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
//        if (driver.findElements(By.id("com.starbucks.mobilecard:id/pay_no_card_header")).isEmpty()) {
//            System.out.println("\n Sign Up was not completed successfully");
//
//            text("******************* FAILED SIGN UP ********************");
//        }
//        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
//        Thread.sleep(500);

        signUp();
        Thread.sleep(500);
        getNavDrawer().click();
        takeScreenshot("fifth");
        Thread.sleep(500);
        text("REWARDS").click();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        if (!driver.findElements(By.id("com.starbucks.mobilecard:id/splash_siren_img")).isEmpty()) {
            System.out.println("\n Sign Up was not completed successfully");
            text("******************* SPLASH SCREEN BLOCKING TESTS ********************");
        }
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        takeScreenshot("sixth");

//        if (getViewById("com.starbucks.mobilecard:id/rewards_overlay_close_button").isDisplayed()){
//            getViewById("com.starbucks.mobilecard:id/rewards_overlay_close_button").click();
//        }
        getViewById("com.starbucks.mobilecard:id/rewards_no_card_fragment_benefits_button").click();
        //Click on Welcome Level
        text("Welcome").click();

        String blankWebview = "//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager[1]/android.widget.LinearLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]";

        takeScreenshot("seventh");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        if (!driver.findElements(By.xpath(blankWebview)).isEmpty()) {
            System.out.println("\n The Welcome webview seems to be empty or missing");
        }
        //Click on Green level
        text("GREEN").click();
        takeScreenshot("eigth");
        if (!driver.findElements(By.xpath(blankWebview)).isEmpty()) {
            System.out.println("\n The Green webview seems to be empty or missing");
        }
        //Click on Gold level
        text("GOLD").click();
        takeScreenshot("ninth");
        if (!driver.findElements(By.xpath(blankWebview)).isEmpty()) {
            System.out.println("\n The Gold webview seems to be empty or missing");
        }
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    //@org.junit.Test

    public void settingAddCreditCardInPaymentMethod() throws Exception {

        signUp();

        Thread.sleep(500);
        assert true : "PAY";
        getNavDrawer().click();
        Thread.sleep(500);

        text("SETTINGS").click();
        getViewById("com.starbucks.mobilecard:id/settings_fragment_payment_methods_tap_area").click();
        assert true : "Add";
        driver.findElementByXPath("//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[1]/android.widget.TextView[1]").click();
        getViewById("com.starbucks.mobilecard:id/credit_card_number").sendKeys("5555555555554444" + enter);
        getViewById("com.starbucks.mobilecard:id/credit_card_exp_date").sendKeys("09/18" + enter);
        getViewById("com.starbucks.mobilecard:id/credit_card_cvn").sendKeys("111" + enter);

        driver.hideKeyboard();
        scroll_to("Phone");
        String phoneNumber = "2062345678";
        String address = "123 Main";

        getViewById("com.starbucks.mobilecard:id/edit_billing_info_phone_number").click();
        getViewById("com.starbucks.mobilecard:id/edit_billing_info_phone_number").sendKeys(phoneNumber + enter);

        getViewById("com.starbucks.mobilecard:id/edit_billing_info_address_1").click();

        for (int i = 0; i < 5; i++) {
            driver.sendKeyEvent(22);
            getViewById("com.starbucks.mobilecard:id/edit_billing_info_address_1").clear();
        }

//        for(int i=20; i >=0; i--)
//        {
//            getViewById("com.starbucks.mobilecard:id/edit_billing_info_address_1").clear();
//
//        }

        getViewById("com.starbucks.mobilecard:id/edit_billing_info_address_1").sendKeys(address);
        Thread.sleep(100);
        //getViewById("com.starbucks.mobilecard:id/add").click();
        driver.findElementByXPath("//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.RelativeLayout[1]/android.view.View[1]/android.support.v7.widget.LinearLayoutCompat[1]/android.widget.TextView[1]").click();
        Assert.assertEquals("View should be back on pay", true, text("1111").isDisplayed());

    }

    @org.junit.Test
    public void setAndDisablePasscode() throws Exception {
        button("GET STARTED").click();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();

        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username + enter);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/account_submit").click();
        getNavDrawer().click();
        Thread.sleep(500);
        text("SETTINGS").click();
        scroll_to("Security");
        getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock_status").click();

        //assert true : "LOCK";
        getViewById("com.starbucks.mobilecard:id/alert_dialog_positive_button").click();
        //assert true : "passcode";
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_2").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_2").click();

        Assert.assertEquals("Confirm passcode should be seen", true, getViewById("com.starbucks.mobilecard:id/fragment_dialog_passcode_helper_text").isDisplayed());
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_2").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_2").click();
        scroll_to("Security");


        String x = getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock_status").getAttribute("checked");
        if (x.equals("false")) {
            System.out.println(enter + "passcode should be checked but " + x + "**********");


        }
        getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock_status").click();
        //assert true : "ON";derItemMop
        getViewById("com.starbucks.mobilecard:id/passcode_dialog_turn_passcode_off").click();
        //assert true : "current";
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_2").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_2").click();
        String y = getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock_status").getAttribute("checked");
        if (y.equals("true")) {
            System.out.println(enter + "passcode should be unchecked but " + y + "**********");
        }
    }

    @org.junit.Test
    public void giftCardsScrollAndSelect() throws Exception {
        button("GET STARTED").click();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username + enter);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/account_submit").click();
        getNavDrawer().click();
        Thread.sleep(500);

        text("GIFT").click();
        getViewById("com.starbucks.mobilecard:id/gift_landing_card_item_image").isDisplayed();
        //scroll_to("//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.HorizontalScrollView[1]/android.widget.LinearLayout[1]/android.widget.TextView[3]");
        //scroll_to("//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.HorizontalScrollView[1]/android.widget.LinearLayout[1]/android.widget.TextView[4]");
        //driver.findElementByXPath("//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.HorizontalScrollView[1]/android.widget.LinearLayout[1]/android.widget.TextView[3]").click();
        scroll_to("THANK YOU");
        //scroll_to("Love");
        getViewById("com.starbucks.mobilecard:id/gift_landing_card_item_image_placeholder").click();
        //driver.findElementByXPath("//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.support.v4.view.ViewPager[1]/android.support.v7.widget.RecyclerView[1]/android.widget.LinearLayout[2]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ImageView[1]").click();
        Assert.assertEquals("Card detail should be seen", true, getViewById("com.starbucks.mobilecard:id/btn_change_card").isDisplayed());
    }

    @org.junit.Test
    public void keepAndDiscardGift() throws Exception {
        button("GET STARTED").click();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username + enter);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/account_submit").click();
        getNavDrawer().click();
        Thread.sleep(500);

        text("GIFT").click();
        getViewById("com.starbucks.mobilecard:id/gift_landing_card_item_image").click();
        getViewById("com.starbucks.mobilecard:id/gift_compose_to_holder").click();
        getViewById("com.starbucks.mobilecard:id/gift_compose_message_to_field").sendKeys("dart@us.com");
        getViewById("com.starbucks.mobilecard:id/gift_compose_message_list_contacts").click();
        text("ADD").click();
        driver.navigate().back();
        getViewById("com.starbucks.mobilecard:id/alert_dialog_positive_button").click();
        driver.navigate().back();
        getViewById("com.starbucks.mobilecard:id/alert_dialog_negative_button").click();
        getViewById("com.starbucks.mobilecard:id/gift_landing_card_item_image").isDisplayed();
    }

    private Boolean checkConnection() throws Exception {

        //TODO:(Darryl) replace this with an actual API call Medhi/ Steven's tool?
        String url = "http://www.cnn.com/robots.txt";

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);

        // add request header
        request.addHeader("User-Agent", USER_AGENT);
        request.addHeader("username", "");
        request.addHeader("password", "");


        HttpResponse response = client.execute(request);

        //System.out.println("\nSending 'GET' request to URL : " + url);
        //System.out.println("Response Code : " +
        //response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        // System.out.println(result.toString());

        if (response.getStatusLine().getStatusCode() == 200 || response.getStatusLine().getStatusCode() == 401) {
            return true;
        } else {
            httpErrorCode = response.getStatusLine().getStatusCode();
            return false;
        }
    }

    public void takeScreenshot(String filename) throws Exception {
        String takeScreenshot = "adb shell screencap -p /sdcard/tmp/" + filename + ".png";

        Process p1 = Runtime.getRuntime().exec(takeScreenshot);

        p1.waitFor();

    }

    @Before
    public void createScreenshotDirectory() throws Exception {
        String createDirectory = "adb shell mkdir /sdcard/tmp";
        Process p = Runtime.getRuntime().exec(createDirectory);
        p.waitFor();
    }

    @After
    public void tearDown() throws Exception {
        //String command ="adb shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > screen.png";
        String takeScreenshot = "adb shell screencap -p /sdcard/tmp/test.png";

        String movePictures = "adb pull /sdcard/tmp/ /Users/dcook/Desktop/images";
        String cleanUp ="adb shell rmdir /sdcard/tmp";

        Process p1 = Runtime.getRuntime().exec(takeScreenshot);
        Process p2 = Runtime.getRuntime().exec(cleanUp);
        Process p3 = Runtime.getRuntime().exec(movePictures);

        p1.waitFor();
        p2.waitFor();
        p3.waitFor();
        //
        driver.quit();
    }

    @org.junit.Test
    //Todo: Screen freeze after sending forgot password
    public void submitForgotPasswordGift() throws Exception {
        button("GET STARTED").click();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/account_submit").click();
        getNavDrawer().click();
        Thread.sleep(500);

        text("GIFT").click();
        getViewById("com.starbucks.mobilecard:id/gift_landing_card_item_image").click();
        getViewById("com.starbucks.mobilecard:id/gift_compose_to_holder").click();
        getViewById("com.starbucks.mobilecard:id/gift_compose_message_to_field").sendKeys("dart@us.com");
        getViewById("com.starbucks.mobilecard:id/gift_compose_message_list_contacts").click();
        text("ADD").click();
        getViewById("com.starbucks.mobilecard:id/button_send").click();
        getViewById("com.starbucks.mobilecard:id/fragment_confirm_forgot_password_en").click();
        getViewById("com.starbucks.mobilecard:id/forgot_password_email").sendKeys(username + enter);
        getViewById("com.starbucks.mobilecard:id/forgot_password_username").sendKeys(username);
        getViewById("com.starbucks.mobilecard:id/button_send").click();
        Assert.assertEquals("Confirm gift seen", true, text("Confirm").isDisplayed());
    }

    //@org.junit.Test
    public void addRemovePhysicalCard() throws Exception {
        signUp();
        Thread.sleep(500);

        getViewById("com.starbucks.mobilecard:id/register_card_get_started_button").click();
        //Assert.assertTrue(doesViewExistId("com.starbucks.mobilecard:id/register_physical_card"));
        getViewById("com.starbucks.mobilecard:id/register_physical_card").click();
        getViewById("com.starbucks.mobilecard:id/new_card_number").sendKeys("7777082960891936");
        getViewById("com.starbucks.mobilecard:id/new_code").click();
        getViewById("com.starbucks.mobilecard:id/new_code").sendKeys("40322058");
        getViewById("com.starbucks.mobilecard:id/add_sbux_card_submit_btn").click();
        getViewById("com.starbucks.mobilecard:id/cards_fragment_snack_pay_button");
        // Assert.assertTrue(doesViewExistId("com.starbucks.mobilecard:id/cards_fragment_snack_pay_button"));

        getViewById("com.starbucks.mobilecard:id/cards_fragment_snack_pay_button").click();
        getViewById("com.starbucks.mobilecard:id/barcode_done_button").click();
        getViewById("com.starbucks.mobilecard:id/pay_fragment_manage_button").click();
        getViewById("com.starbucks.mobilecard:id/fragment_manage_remove_card").click();
        Assert.assertTrue(doesViewExistId("com.starbucks.mobilecard:id/fragment_manage_remove_card"));
        getViewById("com.starbucks.mobilecard:id/alert_dialog_positive_button").click();
        Assert.assertTrue(doesViewExistId("com.starbucks.mobilecard:id/pay_no_card_header"));
        getViewById("com.starbucks.mobilecard:id/pay_no_card_header").click();

    }

    //@org.junit.Test
    public void deleteInboxMessage() throws Exception {
        button("GET STARTED").click();
        assert true : "SIGN UP";
        Thread.sleep(10000);
//        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();
//        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username + enter);
//        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
//        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_sign_in_button").click();

        getNavDrawer().click();
        Thread.sleep(10000);
        text("MESSAGES").click();
        if (find("offers").isDisplayed()) {
            back();
        }
        getNavDrawer().click();
        Thread.sleep(2000);
        text("GIFT").click();
        getNavDrawer().click();
        Thread.sleep(500);
        text("MESSAGES").click();

        assert true : getViewById("com.starbucks.mobilecard:id/inbox_list_item_image").isDisplayed();
        assert true : getViewById("com.starbucks.mobilecard:id/inbox_list_item_title").isDisplayed();

        //getViewById("com.starbucks.mobilecard:id/inbox_list_item_title").
        driver.swipe(1, 1, 200, 200, 10);
        getViewById("com.starbucks.mobilecard:id/sb__action").click();
        //text("UNDO").click();

    }


    @org.junit.Test
    public void autoReload() throws Exception {

        button("GET STARTED").click();

        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(username + enter);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/account_submit").click();

        getViewById("com.starbucks.mobilecard:id/pay_fragment_manage_button").click();

        String ar = getViewById("com.starbucks.mobilecard:id/second_text_view").getAttribute("text");
        if (ar.contains("On")) {
            getViewById("com.starbucks.mobilecard:id/fragment_manage_auto_reload").click();
            getViewById("com.starbucks.mobilecard:id/auto_reload_switch_container").click();
            getViewById("com.starbucks.mobilecard:id/auto_reload_update").click();
        }
        String ar1 = getViewById("com.starbucks.mobilecard:id/second_text_view").getAttribute("text");
        Assert.assertEquals("Auto reload on", ar1.contains("Off"), true);
        getViewById("com.starbucks.mobilecard:id/fragment_manage_auto_reload").click();
        getViewById("com.starbucks.mobilecard:id/auto_reload_switch_container").click();
        getViewById("com.starbucks.mobilecard:id/auto_reload_update").click();
        getViewById("com.starbucks.mobilecard:id/edit_text_combo_box_edit_box").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/alert_dialog_positive_button").click();
        Thread.sleep(1000);
        //TODO ANU: need to check whether autoReload is enabled on main pay
//        driver.findElementByXPath("//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]").click();
//        getViewById("com.starbucks.mobilecard:id/pay_fragment_auto_reload_image").isDisplayed();
//        getViewById("com.starbucks.mobilecard:id/pay_fragment_manage_button").click();
        getViewById("com.starbucks.mobilecard:id/fragment_manage_auto_reload").click();
        getViewById("com.starbucks.mobilecard:id/auto_reload_switch_container").click();
        getViewById("com.starbucks.mobilecard:id/auto_reload_update").click();

    }

    @org.junit.Test
    public void reloadUsingCreditCard() throws Exception {
        signIn();
        getViewById("com.starbucks.mobilecard:id/pay_fragment_reload_button").click();
        String balance1 = getViewById("com.starbucks.mobilecard:id/balance").getAttribute("text");
        String b1 = balance1.substring(1);
        //balance1.trim();
        Float bal1 = Float.valueOf(b1);
        getViewById("com.starbucks.mobilecard:id/reload_confirm").click();
        getViewById("com.starbucks.mobilecard:id/edit_text_combo_box_edit_box").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/alert_dialog_positive_button").click();

        String balance2 = getViewById("com.starbucks.mobilecard:id/pay_fragment_balance_text").getAttribute("text");
        String b2 = balance2.substring(1);
        Float bal2 = Float.valueOf(b2);
        Assert.assertNotEquals("The balance has changed" + balance2, bal1, bal2);


    }

    @org.junit.Test
    public void storesDetail() throws Exception {
        button("GET STARTED").click();
        Thread.sleep(1000);
        getNavDrawer().click();
        Thread.sleep(10000);
        text("STORES").click();

        Assert.assertEquals("Stores title seen but", true, getViewById("com.starbucks.mobilecard:id/toolbar_title_textView").isDisplayed());
        getViewById("com.starbucks.mobilecard:id/stores_menu_search").click();
        getViewById("com.starbucks.mobilecard:id/stores_search_edit_text").sendKeys("Seattle" + enter);
        driver.sendKeyEvent(84);
        Assert.assertEquals("Back to map page", true, getViewById("com.starbucks.mobilecard:id/stores_filter").isDisplayed());

        getViewById("com.starbucks.mobilecard:id/stores_carousel_item_details").click();
        Assert.assertEquals("Should be 5th And Columbia", true, text("Seattle").isDisplayed());
        scroll_to("HOURS");
        Assert.assertEquals("Days of week seen", true, text("Monday").isDisplayed());
        //Assert.assertEquals("Amenities is seen", true ,text("AMENITIES").isDisplayed());

    }
    //Test case deprecated
   // @org.junit.Test
    public void viewTabsInMenu() throws Exception {
        button("GET STARTED").click();
        Thread.sleep(1000);
        getNavDrawer().click();
        Thread.sleep(10000);
        text("MENU").click();

        Assert.assertEquals("Menu Landing screen", true, getViewById("com.starbucks.mobilecard:id/menu_featured_results_item_image").isDisplayed());

        text("CATEGORIES").click();
        Assert.assertEquals("Drinks title seen", true, text("Drinks").isDisplayed());

        text("ALL").click();
        Assert.assertEquals("choices are seen", true, getViewById("com.starbucks.mobilecard:id/menu_all_results_item_category").isDisplayed());

    }
    //Test case Deprecated
    //@org.junit.Test
    public void searchItemsMenu() throws Exception {
        button("GET STARTED").click();
        Thread.sleep(1000);
        getNavDrawer().click();
        Thread.sleep(10000);
        text("MENU").click();

        getViewById("com.starbucks.mobilecard:id/menu_search_list").click();
        //driver.findElementByXPath("//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.RelativeLayout[1]/android.view.View[1]/android.support.v7.widget.LinearLayoutCompat[1]\n").click();
        getViewById("com.starbucks.mobilecard:id/menu_search_edit_text").sendKeys("mocha");
        driver.sendKeyEvent(84);

        Assert.assertEquals("Results should be shown", true, getViewById("com.starbucks.mobilecard:id/menu_all_results_item_category").isDisplayed());

    }
    //Test case deprecated
    //@org.junit.Test
    public void detailScreenMenu() throws Exception {
        button("GET STARTED").click();
        Thread.sleep(1000);
        getNavDrawer().click();
        Thread.sleep(10000);
        text("MENU").click();
        text("CATEGORIES").click();
        text("Frappuccino").click();
        text("ALL").click();
        getViewById("com.starbucks.mobilecard:id/menu_all_results_item_text_container").click();
        scroll_to("Calories");
        String cal = getViewById("com.starbucks.mobilecard:id/product_detail_calories_value").getAttribute("text");
        //Assert.assertEquals("Calories value should be 420", true, cal.contains("420"));
        getViewById("com.starbucks.mobilecard:id/temp_home_layout").click();
        text("Venti").click();
        String cal1 = getViewById("com.starbucks.mobilecard:id/product_detail_calories_value").getAttribute("text");
        Assert.assertEquals("Calories value should be 420", true, cal1 != cal);
    }

    @org.junit.Test
    public void filterStores() throws Exception {
        button("GET STARTED").click();
        Thread.sleep(1000);
        getNavDrawer().click();
        Thread.sleep(10000);
        text("STORES").click();
        Thread.sleep(500);
        getViewById("com.starbucks.mobilecard:id/stores_menu_search").click();
        getViewById("com.starbucks.mobilecard:id/stores_search_edit_text").sendKeys("Boise" + enter);
        driver.sendKeyEvent(84);
        getViewById("com.starbucks.mobilecard:id/stores_filter").click();
        driver.findElementByXPath("//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[1]/android.widget.TextView[1]").click();
        getViewById("com.starbucks.mobilecard:id/stores_filter_apply").click();
        getViewById("com.starbucks.mobilecard:id/stores_menu_list").click();
        Assert.assertEquals("Meridian should be seen", true, text("Meridian").isDisplayed());
    }


    @org.junit.Test
    public void navigateThroughStores() throws Exception {
        button("GET STARTED").click();
        Thread.sleep(1000);
        getNavDrawer().click();
        Thread.sleep(10000);
        text("STORES").click();
        Thread.sleep(1000);

        Assert.assertEquals("Stores title seen but", true, getViewById("com.starbucks.mobilecard:id/toolbar_title_textView").isDisplayed());
        getViewById("com.starbucks.mobilecard:id/stores_menu_search").click();
        getViewById("com.starbucks.mobilecard:id/stores_search_edit_text").sendKeys("Seattle");
        driver.sendKeyEvent(84);
        getViewById("com.starbucks.mobilecard:id/stores_carousel_item_details").click();
        //getViewById("com.starbucks.mobilecard:id/stores_list_item_details").click();
        Assert.assertEquals("Address of store should be seen", true, getViewById("com.starbucks.mobilecard:id/stores_detail_fragment_streetAddress").isDisplayed());
        //Assert.assertEquals("address of store should be seen but", true, getViewById("com.starbucks.mobilecard:id/stores_detail_fragment_streetAddress").isDisplayed());
        back();
        getViewById("com.starbucks.mobilecard:id/stores_menu_list").click();
        getViewById("com.starbucks.mobilecard:id/stores_list_item_inner_details").click();

        Assert.assertEquals("Phone number should be seen but", true, getViewById("com.starbucks.mobilecard:id/stores_detail_fragment_phone").isDisplayed());


    }

    @org.junit.Test
    public void orderItemMop() throws Exception {
        signIn();
        getNavDrawer().click();
        Thread.sleep(10000);
        //String x = driver.findElements(By.id("com.starbucks.mobilecard:id/menu_items_container")).get(3).getText();
        if (text("ORDER").isDisplayed()) {
            text("ORDER").click();
        } else if (text("MENU").isDisplayed()) {
            text("MENU").click();
        }

        getViewById("com.starbucks.mobilecard:id/mop_menu_footer_fake_spinner_store_name").click();
        getViewById("com.starbucks.mobilecard:id/stores_menu_search").click();
        getViewById("com.starbucks.mobilecard:id/stores_search_edit_text").sendKeys("Columbia Center"+enter);
        driver.sendKeyEvent(84);
        Thread.sleep (10000);

        //Searching First for MOP store
        getViewById("com.starbucks.mobilecard:id/stores_menu_list").click();
        text("Columbia Center").click();

        Assert.assertEquals("Store name should be seen", true, getViewById("com.starbucks.mobilecard:id/stores_detail_fragment_name").isDisplayed());
        Assert.assertEquals("Travel time should be seen", true, getViewById("com.starbucks.mobilecard:id/stores_detail_fragment_list_item_travel_time").isDisplayed());
        Assert.assertEquals("Ready in mins should be seen", true, getViewById("com.starbucks.mobilecard:id/stores_detail_fragment_list_item_ready_time").isDisplayed());
        Assert.assertEquals("Travel mode spinner should be seen", true, getViewById("com.starbucks.mobilecard:id/travel_mode_spinner_icon").isDisplayed());

        Assert.assertEquals("Store Address should be seen", true, getViewById("com.starbucks.mobilecard:id/stores_detail_fragment_streetAddress").isDisplayed());
        Assert.assertEquals("Distance to store should be seen", true, getViewById("com.starbucks.mobilecard:id/stores_detail_fragment_distanceAway").isDisplayed());
        Assert.assertEquals("Phone number should be seen", true, getViewById("com.starbucks.mobilecard:id/stores_detail_fragment_phone").isDisplayed());

        getViewById("com.starbucks.mobilecard:id/stores_detail_fragment_select_store").click();

        //Search for product
        getViewById("com.starbucks.mobilecard:id/menu_search_list").click();
        getViewById("com.starbucks.mobilecard:id/menu_search_edit_text").sendKeys("8-Grain Roll"+ enter);

        getViewById("com.starbucks.mobilecard:id/menu_search_item_name").click();
        Assert.assertEquals("Product detail image should be seen", true, getViewById("com.starbucks.mobilecard:id/product_detail_hero_overlay").isDisplayed());
        Assert.assertEquals("Product description should be seen", true, getViewById("com.starbucks.mobilecard:id/product_detail_description").isDisplayed());
        Thread.sleep(10000);
        getViewById("com.starbucks.mobilecard:id/product_detail_add_button").click();

        //Order processing
        getViewById("com.starbucks.mobilecard:id/menu_ongoing_footer_view_order").click();
        //Assert.assertEquals("Delete icon should be displayed", true, getViewById("com.starbucks.mobilecard:id/order_builder_list_item_delete").isDisplayed());
        Assert.assertEquals("Add same item icon should be seen", true, getViewById("com.starbucks.mobilecard:id/order_builder_list_item_single_duplicate").isDisplayed());
        getViewById("com.starbucks.mobilecard:id/order_confirm_submit").click();

        //Verify receipt
        Assert.assertEquals("Thank you ON receipt should be seen", true, getViewById("com.starbucks.mobilecard:id/order_placement_top_text").isDisplayed());
        Assert.assertEquals("Image of order seen", true, getViewById("com.starbucks.mobilecard:id/order_placement_list_item_product_image").isDisplayed());
        Assert.assertEquals("View Receipt should be seen",true, getViewById("com.starbucks.mobilecard:id/order_placement_view_receipt_button").isDisplayed());
        getViewById("com.starbucks.mobilecard:id/order_placement_view_receipt_button").click();

        Assert.assertEquals("Store Info should be seen", true, getViewById("com.starbucks.mobilecard:id/mop_store_header_store_name").isDisplayed());
        Assert.assertEquals("Product information should be seen", true, getViewById("com.starbucks.mobilecard:id/order_builder_list_item_product_name").isDisplayed());
        Assert.assertEquals("ItemPrice should be seen", true, getViewById("com.starbucks.mobilecard:id/order_builder_price_solo_line_item").isDisplayed());
        Assert.assertEquals("Total price text should be seen", true, getViewById("com.starbucks.mobilecard:id/order_confirm_price_footer_total_label").isDisplayed());
        Assert.assertEquals("Total Price of order should be seen", true, getViewById("com.starbucks.mobilecard:id/order_confirm_price_footer_total").isDisplayed());


    }


    public Boolean doesViewExistXPath(String xpath) throws Exception{
        if(!driver.findElements(By.id(xpath)).isEmpty()) {
            System.out.println("\n XPath: "+xpath+" could NOT be found.");
            return false;
        }
        return true;

    }

    public Boolean doesViewExistId(String id){
        if(!driver.findElements(By.id(id)).isEmpty()) {
            System.out.println("\n Id:"+id+" could NOT be found.");
            return false;
        }
        return true;
    }
}
