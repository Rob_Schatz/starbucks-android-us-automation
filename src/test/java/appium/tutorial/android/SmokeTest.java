package appium.tutorial.android;

import appium.tutorial.android.util.AppiumTest;
import org.junit.After;
import org.junit.Before;
//import com.sun.javafx.tools.packager.Log;

import java.util.Calendar;

import static appium.tutorial.android.util.Helpers.*;

/**
 * Created by asripath on 6/4/15.
 */
public class SmokeTest extends AppiumTest {
    @org.junit.Test
    public void signUp() throws Exception {

        button("GET STARTED").click();
        assert true :"SIGN UP";
        String enter="\n";

        getViewById("com.starbucks.mobilecard:id/register_sign_in_register_button").click();
        Thread.sleep((500));
        String firstName = "American";
        String lastName = "Tester";

        driver.findElementById("com.starbucks.mobilecard:id/first_name").sendKeys(firstName + enter);
        driver.findElementById("com.starbucks.mobilecard:id/last_name").sendKeys(lastName + enter);

        Calendar timeSinceEpoch = Calendar.getInstance();
        String email = "a"+ timeSinceEpoch.getTimeInMillis()+"@tester.com";
        getViewById("com.starbucks.mobilecard:id/email").sendKeys(email +enter);


        String password = "Starbucks01*";
        driver.findElementById("com.starbucks.mobilecard:id/password").sendKeys(password+ enter);
        getViewById("com.starbucks.mobilecard:id/confirm_password").sendKeys(password +enter);
        getViewById("com.starbucks.mobilecard:id/zip").sendKeys("98124");
        getViewById("com.starbucks.mobilecard:id/account_submit").click();
        Thread.sleep(500);

        getViewById("com.starbucks.mobilecard:id/register_card_get_started_button").click();

        getViewById("com.starbucks.mobilecard:id/register_digital_card").click();
        Thread.sleep(500);
        assert true: "RELOAD";
        getViewById("com.starbucks.mobilecard:id/pay_fragment_gift_card_image").click();
        assert true: "DONE";
        driver.findElementById("com.starbucks.mobilecard:id/barcode_done_button").click();

        getViewById("com.starbucks.mobilecard:id/pay_fragment_reload_button").click();
        text("Add a Payment Method").click();
        getViewById("com.starbucks.mobilecard:id/spinner_text_main").click();

        getViewById("com.starbucks.mobilecard:id/credit_card_number").sendKeys("4111111111111111");
        getViewById("com.starbucks.mobilecard:id/credit_card_exp_date").sendKeys("09/18");
        getViewById("com.starbucks.mobilecard:id/credit_card_cvn").sendKeys("111");

        driver.hideKeyboard();
        String phoneNumber = "2062345678";
        String address = "123 Main";
        getViewById("com.starbucks.mobilecard:id/edit_billing_info_phone_number").click();

        getViewById("com.starbucks.mobilecard:id/edit_billing_info_phone_number").sendKeys(phoneNumber);
        driver.sendKeyEvent(66);
        getViewById("com.starbucks.mobilecard:id/edit_billing_info_address_1").sendKeys(address);
        getViewById("com.starbucks.mobilecard:id/add").click();
        getViewById("com.starbucks.mobilecard:id/reload_amounts").click();
        text("$25").click();
        getViewById("com.starbucks.mobilecard:id/reload_confirm").click();
        getViewById("com.starbucks.mobilecard:id/fragment_confirm_reload_password").clear();
        getViewById("com.starbucks.mobilecard:id/fragment_confirm_reload_password").sendKeys(password);
        getViewById("com.starbucks.mobilecard:id/alert_dialog_positive_button").click();
        getNavDrawer().click();
        Thread.sleep(500);
        text("SETTINGS").click();
        scroll_to("Sign Out");
        getViewById("com.starbucks.mobilecard:id/settings_inner_fragment_signout_container").click();
        button("SIGN OUT").click();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys(email);
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys("Starbucks01*");
        getViewById("com.starbucks.mobilecard:id/account_submit").click();


    }

    public void takeScreenshot(String filename) throws Exception{
        String takeScreenshot = "adb shell screencap -p /sdcard/tmp/"+filename+".png";

        Process p1 = Runtime.getRuntime().exec(takeScreenshot);

        p1.waitFor();

    }

    @Before
    public void createScreenshotDirectory() throws Exception{
        String createDirectory = "adb shell mkdir /sdcard/tmp";
        Process p = Runtime.getRuntime().exec(createDirectory);
        p.waitFor();

    }

    @After
    public void tearDown() throws Exception {
        //String command ="adb shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > screen.png";
        String takeScreenshot = "adb shell screencap -p /sdcard/tmp/smokeTest.png";

        String movePictures = "adb pull /sdcard/tmp/ /Users/jenkins/JenkinsHome/jobs/Android_3.1.2/workspace/images";
        String cleanUp ="adb shell rmdir /sdcard/tmp";

        Process p1 = Runtime.getRuntime().exec(takeScreenshot);
        Process p2 = Runtime.getRuntime().exec(cleanUp);
        Process p3 = Runtime.getRuntime().exec(movePictures);

        p1.waitFor();
        p2.waitFor();
        p3.waitFor();
        driver.quit();
    }
}
