package appium.tutorial.android;

import appium.tutorial.android.util.AppiumTest;

//import com.sun.tools.internal.ws.api.wsdl.TWSDLExtensible;
import com.thoughtworks.selenium.Wait;

import org.eclipse.jetty.io.View;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;

import javax.annotation.Resource;
import javax.swing.text.html.ListView;
import javax.xml.soap.Text;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.lang.String;
import static appium.tutorial.android.util.Helpers.*;
import static appium.tutorial.android.util.Helpers.wait;

public class AutomatingASimpleActionTest extends AppiumTest {


    @org.junit.Test
    public void one() throws Exception {
        button("GET STARTED").click();
    }


    @org.junit.Test
    public void two() throws Exception {
        button("GET STARTED").click();
        assert true :"SIGN IN";

//        button("SIGN IN").click();
        getViewById("com.starbucks.mobilecard:id/register_sign_in_sign_in_button").click();

        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_password").sendKeys("Abc123");
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_username").sendKeys("dart@us.com");
        getViewById("com.starbucks.mobilecard:id/sign_in_fragment_sign_in_button").click();

        assert true : "PAY";

        getViewById("com.starbucks.mobilecard:id/pay_fragment_gift_card_image").click();
        assert true: "DONE";
        button("DONE").click();

//        //getNavDrawer().click();
        getViewById("android:id/up").click();
        Thread.sleep(500);

        text("SETTINGS").click();
       scroll_to("Privacy");
       getViewById("com.starbucks.mobilecard:id/settings_inner_fragment_privacy_button").click();
        assert true : "PRIVACY";

        back();
        back();
        getViewById("android:id/up").click();
        Thread.sleep(500);
        text("REWARDS").click();
        assert true: "Level";

        getViewById("android:id/up").click();
        Thread.sleep(500);
        text("STORES").click();
        assert true : "Search";

        getViewById("android:id/up").click();
        Thread.sleep(500);
        text("INBOX").click();
        //text inboxNotification = getViewByName("notifications");
        if ((text("notifications").isDisplayed())) {
            getViewById("com.starbucks.mobilecard:id/fragment_inbox_welcome_enable_notifs").click();
        }
        assert true: "Available";

        getViewById("android:id/up").click();
        Thread.sleep(500);
        text("GIFT").click();
        assert true: "Thank You";

        getViewById("android:id/up").click();
        Thread.sleep(500);
        text("SETTINGS").click();
        getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock").click();
        assert true: "personal";
        getViewById("com.starbucks.mobilecard:id/alert_dialog_positive_button").click();

        assert true: "Set your";
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();

        assert true: "Confirm";
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();

        assert true: getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock_status").getText().compareTo("ON");
//        driver.closeApp();
//        driver.launchApp();
        driver.runAppInBackground(5);
//        driver.launchApp();
        //driver.currentActivity();
        assert true: "Enter";
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();

        assert true: "PAY";

        getViewById("android:id/up").click();
        Thread.sleep(500);
        text("SETTINGS").click();
        getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock").click();
        getViewById("com.starbucks.mobilecard:id/first_text_view").click();

        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        getViewById("com.starbucks.mobilecard:id/passcode_keyboard_1").click();
        assert true: getViewById("com.starbucks.mobilecard:id/settings_fragment_passcode_lock_status").getText().compareTo("OFF");



    }


}