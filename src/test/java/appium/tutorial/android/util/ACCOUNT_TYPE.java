package appium.tutorial.android.util;

/**
 * Created by rreed on 7/23/15.
 *
 * List of usernames to use in automation
 */
public enum ACCOUNT_TYPE {



//Accounts list, currently Only US1 works

    US1("dart@us.com", "Abc123"),
 /* CAGREEN("caAccount", "Abc123"),
  CAGOLD("",""),
  UKGREEN("ukAccount", "Abc123"),
  UKGOLD("",""),*/

    ;

    private String mUsername;
    private String mPassword;

    ACCOUNT_TYPE(String username, String password) {
        mUsername = username;
        mPassword = password;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getPassword() {
        return mPassword;
    }
}


